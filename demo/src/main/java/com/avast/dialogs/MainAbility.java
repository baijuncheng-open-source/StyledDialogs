package com.avast.dialogs;

import com.avast.dialogs.slice.DemoActivity;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DemoActivity.class.getName());
    }

}
