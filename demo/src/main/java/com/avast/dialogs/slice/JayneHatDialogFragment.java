/*
 * Copyright 2013 Inmite s.r.o. (www.inmite.eu).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.avast.dialogs.slice;

import com.avast.ohos.dialogs.fragment.SimpleDialogFragment;
import com.avast.ohos.dialogs.iface.IPositiveButtonDialogListener;
import ohos.agp.components.Component;
import com.avast.dialogs.ResourceTable;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * Sample implementation of custom dialog by extending {@link SimpleDialogFragment}.
 *
 * @author David Vávra (david@inmite.eu)
 */
public class JayneHatDialogFragment extends SimpleDialogFragment {

    public JayneHatDialogFragment(Context context) {
        super(context);
    }
    public static void show(Context context) {
        new JayneHatDialogFragment(context).show();
    }

    @Override
    public Builder build(Builder builder) {
        builder.setTitle("Jayne's hat");
        builder.setMessage("A man walks down the street in that hat, people know he's not afraid of anything.");
        builder.setView(LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_view_jayne_hat, null,false),true);
        builder.setPositiveButton("I WANT ONE", new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                for (IPositiveButtonDialogListener listener : getPositiveButtonDialogListeners()) {
                    listener.onPositiveButtonClicked(mRequestCode);
                }
                remove();
            }
        });
        return builder;
    }
}
