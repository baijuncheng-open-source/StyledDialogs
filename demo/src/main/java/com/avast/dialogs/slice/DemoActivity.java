/*
 * Copyright 2013 Inmite s.r.o. (www.inmite.eu).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.dialogs.slice;

import com.avast.ohos.dialogs.date.DatePickerDialog;
import com.avast.ohos.dialogs.time.EasyTimePickerDialog;
import com.avast.ohos.dialogs.time.TimePickerDialog;
import com.avast.ohos.dialogs.fragment.SimpleDialogFragment;
import com.avast.ohos.dialogs.fragment.ProgressDialogFragment;
import com.avast.ohos.dialogs.fragment.ListDialogFragment;
import com.avast.ohos.dialogs.iface.ISimpleDialogListener;
import com.avast.ohos.dialogs.iface.IDateDialogListener;
import com.avast.ohos.dialogs.iface.ISimpleDialogCancelListener;
import com.avast.ohos.dialogs.iface.IListDialogListener;
import com.avast.ohos.dialogs.iface.IMultiChoiceListDialogListener;
import com.avast.ohos.dialogs.util.Common;
import com.avast.dialogs.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.utils.net.Uri;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class DemoActivity extends AbilitySlice implements
        ISimpleDialogListener,
        IDateDialogListener,
        ISimpleDialogCancelListener,
        IListDialogListener,
        IMultiChoiceListDialogListener, DatePickerDialog.OnDateSetListener , TimePickerDialog.OnTimeSetListener,EasyTimePickerDialog.OnTimeSetListener {

    private static final int REQUEST_PROGRESS = 1;
    private static final int REQUEST_LIST_SIMPLE = 9;
    private static final int REQUEST_LIST_MULTIPLE = 10;
    private static final int REQUEST_LIST_SINGLE = 11;
    private static final int REQUEST_DATE_PICKER = 12;
    private static final int REQUEST_TIME_PICKER = 13;
    private static final int REQUEST_SIMPLE_DIALOG = 42;
    private PopupDialog mMenuDialog;
    private Image right_title_image;
    private boolean isDarkTheme = false;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private EasyTimePickerDialog etpd;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_main);
        Button message_dialog = (Button) findComponentById(ResourceTable.Id_message_dialog);
        message_dialog.setClickedListener(new Component.ClickedListener()  {
            @Override
            public void onClick(Component component) {
                SimpleDialogFragment.createBuilder(DemoActivity.this)
                        .setMessage("Love. Can know all the math in the \'verse but take a boat in the air that you don\'t " +
                                "love? She\'ll shake you off just as sure as a turn in the worlds. Love keeps her in the air when " +
                                "she oughtta fall down...tell you she\'s hurtin\' \'fore she keens...makes her a home.")
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button message_title_dialog = (Button) findComponentById(ResourceTable.Id_message_title_dialog);
        message_title_dialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                SimpleDialogFragment.createBuilder(DemoActivity.this)
                        .setTitle("More Firefly quotes:").setMessage
                        ("Wash: \"Psychic, though? That sounds like something out of science fiction.\"\n\nZoe: \"We live" +
                                " " +
                                "in a space ship, dear.\"\nWash: \"Here lies my beloved Zoe, " +
                                ("my autumn flower ... somewhat less attractive now that she's all corpsified and gross" +
                                        ".\"\n\nRiver Tam: \"Also? I can kill you with my brain.\"\n\nKayle: \"Going on a year now, nothins twixed my neathers not run on batteries.\" \n" +
                                        "Mal: \"I can't know that.\" \n" +
                                        "Jayne: \"I can stand to hear a little more.\"\n\nWash: \"I've been under fire before. " +
                                        "Well ... I've been in a fire. Actually, I was fired. I can handle myself.\""))
                        .setNegativeButtonText("Close")
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button message_title_buttons_dialog = (Button) findComponentById(ResourceTable.Id_message_title_buttons_dialog);
        message_title_buttons_dialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                SimpleDialogFragment.createBuilder(DemoActivity.this)
                                .setTitle("Do you like this quote?")
                                .setMessage("Jayne: \"Shiny. Let's be bad guys.\"")
                                .setPositiveButtonText("Love")
                                .setNegativeButtonText("Hate")
                                .setNeutralButtonText("WTF?")
                                .setRequestCode(REQUEST_SIMPLE_DIALOG)
                                .useDarkTheme(isDarkTheme)
                                .show();
            }
        });
        Button long_buttons = (Button) findComponentById(ResourceTable.Id_long_buttons);
        long_buttons.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                SimpleDialogFragment.createBuilder(DemoActivity.this).setMessage("How will you decide?")
                        .setPositiveButtonText("Time for some thrillin' heroics!").setNegativeButtonText("Misbehave")
                        .setNeutralButtonText("Keep flying")
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button progress_dialog = (Button) findComponentById(ResourceTable.Id_progress_dialog);
        progress_dialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ProgressDialogFragment.createBuilder(DemoActivity.this)
                        .setMessage("Mal: I\'m just waiting to see if I pass out. Long story.")
                        .setRequestCode(REQUEST_PROGRESS)
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button list_dialog_simple = (Button) findComponentById(ResourceTable.Id_list_dialog_simple);
        list_dialog_simple.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ListDialogFragment
                        .createBuilder(DemoActivity.this)
                        .setTitle("Your favorite character:")
                        .setItems(new String[]{"Jayne", "Malcolm", "Kaylee",
                                "Wash", "Zoe", "River"})
                        .setRequestCode(REQUEST_LIST_SIMPLE)
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });

        Button list_dialog_single = (Button) findComponentById(ResourceTable.Id_list_dialog_single);
        list_dialog_single.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ListDialogFragment
                        .createBuilder(DemoActivity.this)
                        .setTitle("Your favorite character:")
                        .setItems(new String[]{"Jayne", "Malcolm", "Kaylee",
                                "Wash", "Zoe", "River"})
                        .setRequestCode(REQUEST_LIST_SINGLE)
                        .setChoiceMode(1)
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button list_dialog_multiple = (Button) findComponentById(ResourceTable.Id_list_dialog_multiple);
        list_dialog_multiple.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ListDialogFragment
                        .createBuilder(DemoActivity.this)
                        .setTitle("Your favorite character:")
                        .setItems(new String[]{"Jayne", "Malcolm", "Kaylee",
                                "Wash", "Zoe", "River"})
                        .setRequestCode(REQUEST_LIST_MULTIPLE)
                        .setChoiceMode(2)
                        .setCheckedItems(new int[]{1, 3})
                        .useDarkTheme(isDarkTheme)
                        .show();
            }
        });
        Button custom_dialog = (Button) findComponentById(ResourceTable.Id_custom_dialog);
        custom_dialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                JayneHatDialogFragment.show(DemoActivity.this);
            }
        });
        Button time_picker = (Button) findComponentById(ResourceTable.Id_time_picker);
        time_picker.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Calendar now = Calendar.getInstance();
                if (tpd == null) {
                    tpd = TimePickerDialog.newInstance(getContext(),DemoActivity.this,now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                } else {
                    tpd.initialize(
                            DemoActivity.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            now.get(Calendar.SECOND),
                           false
                    );
                }
                tpd.setThemeDark(isDarkTheme);
                tpd.vibrate(true);
                tpd.dismissOnPause(true);
                tpd.setVersion(TimePickerDialog.Version.VERSION_2 );
                tpd.setAccentColor(Color.getIntColor("#FFC66D"));
                tpd.setOnCancelListener(dialogInterface -> {
                    tpd = null;
                    Common.toastDialog("Time Cancelled",DemoActivity.this);
                });
                tpd.show();
            }
        });

        Button date_picker = (Button) findComponentById(ResourceTable.Id_date_picker);
        date_picker.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Calendar now = Calendar.getInstance();
                if (dpd == null) {
                    dpd = DatePickerDialog.newInstance(
                            getContext(),
                            DemoActivity.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                } else {
                    dpd.initialize(
                            DemoActivity.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                }
                dpd.setAlignment(LayoutAlignment.CENTER);
                dpd.setThemeDark(isDarkTheme);
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setAccentColor(Color.getIntColor("#FFC66D"));
                dpd.setOnCancelListener(dialog -> {
                    Common.toastDialog("Date Cancelled",DemoActivity.this);
                    dpd = null;
                });
                dpd.show();
            }
        });

        right_title_image = (Image) findComponentById(ResourceTable.Id_right_title_image);
        right_title_image.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onCreateOptionsMenu();
                mMenuDialog.show();
            }
        });

    }

    // IListDialogListener

    @Override
    public void onListItemSelected(CharSequence value, int number, int requestCode) {
        if (requestCode == REQUEST_LIST_SIMPLE || requestCode == REQUEST_LIST_SINGLE) {
            Common.toastDialog("Selected: " + value,DemoActivity.this);
        }
    }

    @Override
    public void onListItemsSelected(CharSequence[] values, int[] selectedPositions, int requestCode) {
        if (requestCode == REQUEST_LIST_MULTIPLE) {
            StringBuilder sb = new StringBuilder();
            for (CharSequence value : values) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(value);
            }
            Common.toastDialog("Selected: " + sb.toString(),DemoActivity.this);
        }
    }

    @Override
    public void onCancelled(int requestCode) {
        ToastDialog toastDialog = new ToastDialog(getContext());
        toastDialog.setDuration(200);
        switch (requestCode) {
            case REQUEST_SIMPLE_DIALOG:
                Common.toastDialog("Dialog cancelled",DemoActivity.this);
                break;
            case REQUEST_PROGRESS:
                Common.toastDialog("Progress dialog cancelled",DemoActivity.this);
                break;
            case REQUEST_LIST_SIMPLE:
            case REQUEST_LIST_SINGLE:
            case REQUEST_LIST_MULTIPLE:
                Common.toastDialog("Nothing selected",DemoActivity.this);
                break;
            case REQUEST_DATE_PICKER:
                Common.toastDialog("Date picker cancelled",DemoActivity.this);
                break;
            case REQUEST_TIME_PICKER:
                Common.toastDialog("Time picker cancelled",DemoActivity.this);
                break;
        }
        toastDialog.show();
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == REQUEST_SIMPLE_DIALOG) {
            Common.toastDialog("Positive button clicked",DemoActivity.this);
        }
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        if (requestCode == REQUEST_SIMPLE_DIALOG) {
            Common.toastDialog("Negative button clicked",DemoActivity.this);
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
        if (requestCode == REQUEST_SIMPLE_DIALOG) {
            Common.toastDialog("Neutral button clicked",DemoActivity.this);
        }
    }

    @Override
    public void onNegativeButtonClicked(int resultCode, Date date) {
        String text = "";
        if (resultCode == REQUEST_DATE_PICKER) {
            text = "Date ";
        } else if (resultCode == REQUEST_TIME_PICKER) {
            text = "Time ";
        }
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT);
        Common.toastDialog(text + "Cancelled " + dateFormat.format(date),DemoActivity.this);
    }

    @Override
    public void onPositiveButtonClicked(int resultCode, Date date) {
        String text = "";
        if (resultCode == REQUEST_DATE_PICKER) {
            text = "Date ";
        } else if (resultCode == REQUEST_TIME_PICKER) {
            text = "Time ";
        }
        DateFormat dateFormat = DateFormat.getDateTimeInstance();
        Common.toastDialog(text + "Success! " + dateFormat.format(date),DemoActivity.this);
    }

    // menu
    public void onCreateOptionsMenu() {
        if (mMenuDialog != null) {
            return;
        }
        Component menu = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_menu, null, false);
        Text theme_change = (Text) menu.findComponentById(ResourceTable.Id_theme_change);
        Text about = (Text) menu.findComponentById(ResourceTable.Id_about);
        about.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent i = new Intent();
                i.setUri(Uri.parse("https://gitee.com/baijuncheng-open-source/StyledDialogs"));
                startAbility(i);
                mMenuDialog.hide();
            }
        });

        theme_change.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isDarkTheme) {
                    setTheme(ResourceTable.Color_sdl_pressed_dark);
                    theme_change.setText("Use Dark Theme");
                    Common.toastDialog("Light theme set",DemoActivity.this);
                    isDarkTheme = false;
                } else {
                    setTheme(ResourceTable.Color_sdl_pressed_dark);
                    theme_change.setText("Use Light Theme");
                    Common.toastDialog("Dark theme set",DemoActivity.this);
                    isDarkTheme = true;
                }
                mMenuDialog.hide();
            }
        });
        if (mMenuDialog == null) {
            mMenuDialog = new PopupDialog(DemoActivity.this, right_title_image);
        }
        mMenuDialog.setCustomComponent(menu);
        mMenuDialog.setDialogListener(new BaseDialog.DialogListener() {
            @Override
            public boolean isTouchOutside() {
                mMenuDialog.hide();
                return false;
            }
        });
    }
    @Override
    protected void onStop() {
        super.onStop();
        dpd = null;
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (dpd != null) {
            dpd.onBackground(dpd);
            dpd = null;
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String date = "Date Success!" + year + "年" + (++monthOfYear) + "月" + dayOfMonth + "日";
        Common.toastDialog(date,DemoActivity.this);
        dpd = null;
    }

    @Override
    public void onTimeSet(TimePickerDialog dialog, int hourOfDay, int minute, int second) {
        String date = "Time Success!" + hourOfDay + "时" + (++minute) + "分" + second + "秒";
        Common.toastDialog(date,DemoActivity.this);
        tpd = null;
    }

    @Override
    public void onEasyTimeShow(TimePickerDialog dialog, boolean showEasyTime) {
        dpd = null;
        Calendar now = Calendar.getInstance();
        if (etpd == null) {
            etpd = EasyTimePickerDialog.newInstance(
                    getContext(),
                    DemoActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
        } else {
            etpd.initialize(
                    DemoActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    false
            );
        }
        etpd.setThemeDark(isDarkTheme);
        etpd.vibrate(true);
        etpd.dismissOnPause(true);
        etpd.setAccentColor(Color.getIntColor("#FFC66D"));
        etpd.setOnCancelListener(dialogInterface -> {
            etpd = null;
            Common.toastDialog("Time Cancelled",DemoActivity.this);
        });
        etpd.show();
    }

    @Override
    public void onEasyTimeSet(EasyTimePickerDialog dialog,int year,int month,int day, int hourOfDay, int minute,String am_pm) {
        String time = "Time Success!" + year + "年" + month + "月" + day + "日" + " " + am_pm + " " + hourOfDay + "时：" + minute + "分";
        Common.toastDialog(time,DemoActivity.this);
        etpd = null;
    }

    @Override
    public void onTimeShow(EasyTimePickerDialog dialog, boolean showTime) {
        etpd = null;
        Calendar now = Calendar.getInstance();
        if (tpd == null) {
            tpd = TimePickerDialog.newInstance(
                    getContext(),
                    DemoActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
        } else {
            tpd.initialize(
                    DemoActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    false
            );
        }
        tpd.setThemeDark(isDarkTheme);
        tpd.vibrate(true);
        tpd.dismissOnPause(true);
        tpd.setVersion(TimePickerDialog.Version.VERSION_2 );
        tpd.setAccentColor(Color.getIntColor("#FFC66D"));
        tpd.setOnCancelListener(dialogInterface -> {
            tpd = null;
            Common.toastDialog("Time Cancelled",DemoActivity.this);
        });
        tpd.show();
    }
}
