package com.avast.ohos.dialogs.fragment;

import java.util.List;

import com.avast.ohos.dialogs.core.BaseDialogBuilder;
import com.avast.ohos.dialogs.core.BaseDialogFragment;
import com.avast.ohos.dialogs.iface.IListDialogListener;
import com.avast.ohos.dialogs.iface.IMultiChoiceListDialogListener;
import com.avast.ohos.dialogs.iface.ISimpleDialogCancelListener;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.utils.PacMap;
import com.avast.ohos.dialogs.ResourceTable;
import com.avast.ohos.dialogs.adapter.MultipleBaseListDialogProvider;
import com.avast.ohos.dialogs.adapter.NormalBaseListDialogProvider;
import com.avast.ohos.dialogs.adapter.SingleBaseListDialogProvider;

/**
 * Dialog with a list of options.
 * <p/>
 * Implement {@link IListDialogListener} to handle selection of single and no choice
 * modes. Implement {@link IMultiChoiceListDialogListener} to handle selection of
 * multi choice.
 */
public class ListDialogFragment extends BaseDialogFragment {

    RecycleItemProvider itemProvider = null;

    public ListDialogFragment(Context context) {
        super(context);
    }

    public static SimpleListDialogBuilder createBuilder(Context context) {
        return new SimpleListDialogBuilder(context);
    }

    private RecycleItemProvider prepareAdapter(final int itemLayoutId,boolean isDarkTheme) {
        switch (getMode()) {
            case 0:
                itemProvider = new NormalBaseListDialogProvider(mContext, itemLayoutId, getItems(),isDarkTheme);
                break;
            case 1:
                itemProvider = new SingleBaseListDialogProvider(mContext, itemLayoutId, getItems(), getSelectedItem(),isDarkTheme);
                break;
            case 2:
                itemProvider = new MultipleBaseListDialogProvider(mContext, itemLayoutId, getItems(), getCheckedItems(),isDarkTheme);
                break;
        }
        return itemProvider;
    }

    private void buildMultiChoice(Builder builder) {
        builder.setItems(
                prepareAdapter(ResourceTable.Layout_sdl_list_item_multichoice,isDarkTheme),
                new ListContainer.ItemClickedListener() {
                    @Override
                    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                        listContainer.setHeight(listContainer.getHeight());
                        ((MultipleBaseListDialogProvider) itemProvider).setCheckedItem(i);
                    }
                });
    }

    private void buildSingleChoice(Builder builder) {
        builder.setItems(
                prepareAdapter(ResourceTable.Layout_sdl_list_item_singlechoice,isDarkTheme),
                new ListContainer.ItemClickedListener() {
                    @Override
                    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                        listContainer.setHeight(listContainer.getHeight());
                        ((SingleBaseListDialogProvider) itemProvider).setSelectedItem(i);
                    }
                });
    }

    private void buildNormalChoice(Builder builder) {
        builder.setItems(
                prepareAdapter(ResourceTable.Layout_sdl_list_item,isDarkTheme),
                new ListContainer.ItemClickedListener() {
                    @Override
                    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                        for (IListDialogListener listener : getSingleDialogListeners()) {
                            listener.onListItemSelected(getItems()[i], i, mRequestCode);
                        }
                        remove();
                    }
                });
    }

    @Override
    protected Builder build(Builder builder) {
        final String title = getTitle();
        if (!SimpleDialogFragment.isTextEmpty(title)) {
            builder.setTitle(title);
        }
        if (!SimpleDialogFragment.isTextEmpty(getNegativeButtonText())) {
            builder.setNegativeButton(getNegativeButtonText(), new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    for (ISimpleDialogCancelListener listener : getCancelListeners()) {
                        listener.onCancelled(mRequestCode);
                    }
                    remove();
                }
            });
        }

        //confirm button makes no sense when CHOICE_MODE_NONE
        if (getMode() != 0) {
            Component.ClickedListener positiveButtonClickListener = null;
            switch (getMode()) {
                case 2:
                    positiveButtonClickListener = new Component.ClickedListener() {
                        @Override
                        public void onClick(Component view) {
                            // prepare multiple results
                            final int[] checkedPositions = ((MultipleBaseListDialogProvider) itemProvider).getCheckedItem();
                            final String[] items = getItems();
                            final String[] checkedValues = new String[checkedPositions.length];
                            int i = 0;
                            for (int checkedPosition : checkedPositions) {
                                if (checkedPosition >= 0 && checkedPosition < items.length) {
                                    checkedValues[i++] = items[checkedPosition];
                                }
                            }

                            for (IMultiChoiceListDialogListener listener : getMutlipleDialogListeners()) {
                                listener.onListItemsSelected(checkedValues, checkedPositions, mRequestCode);
                            }
                            remove();
                        }
                    };
                    break;
                case 1:
                    positiveButtonClickListener = new Component.ClickedListener() {
                        @Override
                        public void onClick(Component view) {
                            // prepare single result
                            int selectedPosition = ((SingleBaseListDialogProvider) itemProvider).getSelectedItem();
                            final String[] items = getItems();

                            // either item is selected or dialog is cancelled
                            if (selectedPosition != -1) {
                                for (IListDialogListener listener : getSingleDialogListeners()) {
                                    listener.onListItemSelected(items[selectedPosition], selectedPosition, mRequestCode);
                                }
                            } else {
                                for (ISimpleDialogCancelListener listener : getCancelListeners()) {
                                    listener.onCancelled(mRequestCode);
                                }
                            }
                            remove();
                        }
                    };
                    break;
            }

            String positiveButton = getPositiveButtonText();
            if (SimpleDialogFragment.isTextEmpty(getPositiveButtonText())) {
                //we always need confirm button when CHOICE_MODE_SINGLE or CHOICE_MODE_MULTIPLE
                try {
                    positiveButton = mContext.getResourceManager().getElement(ResourceTable.String_string_ok).getString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            builder.setPositiveButton(positiveButton, positiveButtonClickListener);
        }

        // prepare list and its item click listener
        final String[] items = getItems();
        if (items != null && items.length > 0) {
            final int mode = getMode();
            switch (mode) {
                case 2:
                    buildMultiChoice(builder);
                    break;
                case 1:
                    buildSingleChoice(builder);
                    break;
                case 0:
                    buildNormalChoice(builder);
                    break;
            }
        }

        return builder;
    }

    /**
     * Get dialog listeners.
     * There might be more than one listener.
     *
     * @return Dialog listeners
     * @since 2.1.0
     */
    private List<IListDialogListener> getSingleDialogListeners() {
        return getDialogListeners(IListDialogListener.class);
    }

    /**
     * Get dialog listeners.
     * There might be more than one listener.
     *
     * @return Dialog listeners
     * @since 2.1.0
     */
    private List<IMultiChoiceListDialogListener> getMutlipleDialogListeners() {
        return getDialogListeners(IMultiChoiceListDialogListener.class);
    }

    private String getTitle() {
        return getArguments().getString("title");
    }

    private int getMode() {
        return getArguments().getIntValue("choiceMode");
    }

    private String[] getItems() {
        return getArguments().getStringArray("items");
    }

    private int getSelectedItem() {
        return getArguments().getIntValue("singleCheckedItem");
    }

    private int[] getCheckedItems() {
        return getArguments().getIntValueArray("checkedItems");
    }

    private String getPositiveButtonText() {
        return getArguments().getString("positive_button");
    }

    private String getNegativeButtonText() {
        return getArguments().getString("negative_button");
    }

    public static class SimpleListDialogBuilder extends BaseDialogBuilder<SimpleListDialogBuilder> {

        private String title;

        private String[] items;

        private int mode;
        private int[] checkedItems;

        private int selectedItem = -1;

        private String cancelButtonText;
        private String confirmButtonText;


        public SimpleListDialogBuilder(Context context) {
            super(context, ListDialogFragment.class);
        }

        @Override
        protected SimpleListDialogBuilder self() {
            return this;
        }

        private ResourceManager getResources() {
            return mContext.getResourceManager();
        }

        public SimpleListDialogBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public SimpleListDialogBuilder setTitle(int titleResID) {
            try {
                this.title = getResources().getElement(titleResID).getString();
            } catch (Exception e) {
                e.printStackTrace();
                this.title = "";
            }
            return this;
        }


        /**
         * Positions of item that should be pre-selected
         * Valid for setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE)
         *
         * @param positions list of item positions to mark as checked
         * @return builder
         */
        public SimpleListDialogBuilder setCheckedItems(int[] positions) {
            this.checkedItems = positions;
            return this;
        }

        /**
         * Position of item that should be pre-selected
         * Valid for setChoiceMode(AbsListView.CHOICE_MODE_SINGLE)
         *
         * @param position item position to mark as selected
         * @return builder
         */
        public SimpleListDialogBuilder setSelectedItem(int position) {
            this.selectedItem = position;
            return this;
        }

        public SimpleListDialogBuilder setChoiceMode(int choiceMode) {
            this.mode = choiceMode;
            return this;
        }

        public SimpleListDialogBuilder setItems(String[] items) {
            this.items = items;
            return this;
        }

        public SimpleListDialogBuilder setItems(int itemsArrayResID) {
            try {
                this.items = getResources().getElement(itemsArrayResID).getStringArray();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        public SimpleListDialogBuilder setConfirmButtonText(String text) {
            this.confirmButtonText = text;
            return this;
        }

        public SimpleListDialogBuilder setConfirmButtonText(int confirmBttTextResID) {
            try {
                this.confirmButtonText = getResources().getElement(confirmBttTextResID).getString();
            } catch (Exception e) {
                e.printStackTrace();
                this.confirmButtonText = "";
            }
            return this;
        }

        public SimpleListDialogBuilder setCancelButtonText(String text) {
            this.cancelButtonText = text;
            return this;
        }

        public SimpleListDialogBuilder setCancelButtonText(int cancelBttTextResID) {
            try {
                this.cancelButtonText = getResources().getElement(cancelBttTextResID).getString();
            } catch (Exception e) {
                e.printStackTrace();
                this.cancelButtonText = "";
            }
            return this;
        }

        @Override
        protected PacMap prepareArguments() {
            PacMap args = new PacMap();
            args.putString("title", title);
            args.putString("positive_button", confirmButtonText);
            args.putString("negative_button", cancelButtonText);
            args.putStringArray("items", items);
            args.putIntValueArray("checkedItems", checkedItems);
            args.putIntValue("singleCheckedItem", selectedItem);
            args.putIntValue("choiceMode", mode);
            return args;
        }
    }

}
