/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.avast.ohos.dialogs.time;

import com.avast.ohos.dialogs.HapticFeedbackController;
import com.avast.ohos.dialogs.ResourceTable;
import com.avast.ohos.dialogs.Utils;
import com.avast.ohos.dialogs.core.DialogBase;
import com.avast.ohos.dialogs.core.DialogInterface;
import com.avast.ohos.dialogs.core.DialogInterface.OnBackgroundListener;
import com.avast.ohos.dialogs.core.ResourceUtils;
import com.avast.ohos.dialogs.time.RadialPickerLayout.OnValueSelectedListener;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.Button;
import ohos.agp.components.Image;

import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;
import ohos.utils.PacMap;

import java.util.Calendar;
import java.util.Date;

/**
 * Dialog to set a time.
 */
public class EasyTimePickerDialog extends DialogBase implements
        OnValueSelectedListener, TimePickerController, OnBackgroundListener {

    public static final int SECOND_INDEX = 2;

    // Delay before starting the pulse animation, in ms.
    private OnTimeSetListener mCallback;
    private HapticFeedbackController mHapticFeedbackController;
    private Button mCancelButton;
    private Button mOkButton;

    private boolean mThemeDark;
    private boolean mThemeDarkChanged;
    private boolean mVibrate;
    private Integer mAccentColor = null;
    private boolean mDismissOnPause;
    private boolean mEnableSeconds;
    private boolean mEnableMinutes;
    private int mOkResid;
    private String mOkString;
    private Integer mOkColor = null;
    private int mCancelResid;
    private String mCancelString;
    private Integer mCancelColor = null;
    private  Calendar calendar;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minutes;
    private String am_pm;
    private boolean showTime = true;
    /**
     * The callback interface used to indicate the user is done filling in
     * the time (they clicked on the 'Set' button).
     */
    public interface OnTimeSetListener {

        /**
         * @param dialog The view associated with this listener.
         * @param hourOfDay The hour that was set.
         * @param minute The minute that was set.
         */
        void onEasyTimeSet(EasyTimePickerDialog dialog,int year,int month,int day, int hourOfDay, int minute,String am_pm);
        void onTimeShow(EasyTimePickerDialog dialog, boolean showTime);

    }

    public EasyTimePickerDialog(Context context) {
        super(context);
    }

    /**
     * Create a new TimePickerDialog instance with a given intial selection
     * @param callback     How the parent is notified that the time is set.
     * @param hourOfDay    The initial hour of the dialog.
     * @param minute       The initial minute of the dialog.
     * @param second       The initial second of the dialog.
     * @param is24HourMode True to render 24 hour mode, false to render AM / PM selectors.
     * @return a new TimePickerDialog instance.
     */
    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
    public static EasyTimePickerDialog newInstance(Context context, OnTimeSetListener callback,
                                                   int hourOfDay, int minute, int second, boolean is24HourMode) {
        EasyTimePickerDialog ret = new EasyTimePickerDialog(context);
        ret.initialize(callback, hourOfDay, minute, second, is24HourMode);
        return ret;
    }

    /**
     * Create a new TimePickerDialog instance with a given initial selection
     * @param callback     How the parent is notified that the time is set.
     * @param hourOfDay    The initial hour of the dialog.
     * @param minute       The initial minute of the dialog.
     * @param is24HourMode True to render 24 hour mode, false to render AM / PM selectors.
     * @return a new TimePickerDialog instance.
     */
    public static EasyTimePickerDialog newInstance(Context context, OnTimeSetListener callback,
                                                   int hourOfDay, int minute, boolean is24HourMode) {
        return EasyTimePickerDialog.newInstance(context, callback, hourOfDay, minute, 0, is24HourMode);
    }

    /**
     * Create a new TimePickerDialog instance initialized to the current system time
     * @param callback     How the parent is notified that the time is set.
     * @param is24HourMode True to render 24 hour mode, false to render AM / PM selectors.
     * @return a new TimePickerDialog instance.
     */
    @SuppressWarnings({"unused", "SameParameterValue", "WeakerAccess"})
    public static EasyTimePickerDialog newInstance(Context context, OnTimeSetListener callback, boolean is24HourMode) {
        Calendar now = Calendar.getInstance();
        return EasyTimePickerDialog.newInstance(context, callback, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), is24HourMode);
    }

    public void initialize(OnTimeSetListener callback,
            int hourOfDay, int minute, int second, boolean is24HourMode) {
        mCallback = callback;
        mThemeDark = false;
        mThemeDarkChanged = false;
        mVibrate = true;
        mDismissOnPause = false;
        mEnableSeconds = false;
        mEnableMinutes = true;
        mOkResid = ResourceTable.String_mdtp_ok;
        mCancelResid = ResourceTable.String_mdtp_cancel;
        calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minutes = calendar.get(Calendar.MINUTE);
        am_pm = calendar.get(Calendar.AM_PM) == 0 ? "上午" : "下午";
    }

    /**
     * Set a dark or light theme. NOTE: this will only take effect for the next onCreateView.
     */
    public void setThemeDark(boolean dark) {
        mThemeDark = dark;
        mThemeDarkChanged = true;
    }

    /**
     * Set the accent color of this dialog
     * @param color the accent color you want
     */
    @SuppressWarnings("unused")
    public void setAccentColor(String color) {
        mAccentColor = Color.getIntColor(color);
    }

    /**
     * Set the accent color of this dialog
     * @param color the accent color you want
     */
    public void setAccentColor(/*@ColorInt*/ int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        rgbColor.setAlpha(255);
        mAccentColor = rgbColor.asArgbInt();
    }

    /**
     * Set the text color of the OK button
     * @param color the color you want
     */
    @SuppressWarnings("unused")
    public void setOkColor(String color) {
        mOkColor = Color.getIntColor(color);
    }

    /**
     * Set the text color of the OK button
     * @param color the color you want
     */
    @SuppressWarnings("unused")
    public void setOkColor(/*@ColorInt*/ int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        rgbColor.setAlpha(255);
        mOkColor = rgbColor.asArgbInt();
    }

    /**
     * Set the text color of the Cancel button
     * @param color the color you want
     */
    @SuppressWarnings("unused")
    public void setCancelColor(String color) {
        mCancelColor = Color.getIntColor(color);
    }

    /**
     * Set the text color of the Cancel button
     * @param color the color you want
     */
    @SuppressWarnings("unused")
    public void setCancelColor(/*@ColorInt*/ int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        rgbColor.setAlpha(255);
        mCancelColor = rgbColor.asArgbInt();
    }

    @Override
    public boolean isThemeDark() {
        return mThemeDark;
    }

    @Override
    public boolean is24HourMode() {
        return true;
    }

    @Override
    public int getAccentColor() {
        return mAccentColor;
    }

    /**
     * Set whether the device should vibrate when touching fields
     * @param vibrate true if the device should vibrate when touching a field
     */
    public void vibrate(boolean vibrate) {
        mVibrate = vibrate;
    }

    /**
     * Set whether the picker should dismiss itself when it's pausing or whether it should try to survive an orientation change
     * @param dismissOnPause true if the picker should dismiss itself
     */
    public void dismissOnPause(boolean dismissOnPause) {
        mDismissOnPause = dismissOnPause;
    }

    /**
     * Set whether an additional picker for seconds should be shown
     * Will enable minutes picker as well if seconds picker should be shown
     * @param enableSeconds true if the seconds picker should be shown
     */
    public void enableSeconds(boolean enableSeconds) {
        if (enableSeconds) mEnableMinutes = true;
        mEnableSeconds = enableSeconds;
    }

    /**
     * Set whether the picker for minutes should be shown
     * Will disable seconds if minutes are disbled
     * @param enableMinutes true if minutes picker should be shown
     */
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void enableMinutes(boolean enableMinutes) {
        if (!enableMinutes) mEnableSeconds = false;
        mEnableMinutes = enableMinutes;
    }

    public void setOnTimeSetListener(OnTimeSetListener callback) {
        mCallback = callback;
    }

    /**
     * Set the label for the Ok button (max 12 characters)
     * @param okString A literal String to be used as the Ok button label
     */
    @SuppressWarnings("unused")
    public void setOkText(String okString) {
        mOkString = okString;
    }

    /**
     * Set the label for the Ok button (max 12 characters)
     * @param okResid A resource ID to be used as the Ok button label
     */
    @SuppressWarnings("unused")
    public void setOkText(/*@StringRes*/ int okResid) {
        mOkString = null;
        mOkResid = okResid;
    }

    /**
     * Set the label for the Cancel button (max 12 characters)
     * @param cancelString A literal String to be used as the Cancel button label
     */
    @SuppressWarnings("unused")
    public void setCancelText(String cancelString) {
        mCancelString = cancelString;
    }

    /**
     * Set the label for the Cancel button (max 12 characters)
     * @param cancelResid A resource ID to be used as the Cancel button label
     */
    @SuppressWarnings("unused")
    public void setCancelText(/*@StringRes*/ int cancelResid) {
        mCancelString = null;
        mCancelResid = cancelResid;
    }

    /**
     * Get a reference to the OnTimeSetListener callback
     * @return OnTimeSetListener the callback
     */
    @SuppressWarnings("unused")
    public OnTimeSetListener getOnTimeSetListener() {
        return mCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Component view = createCompnent(LayoutScatter.getInstance(context), null, null);
        DirectionalLayout layout = new DirectionalLayout(context);
        layout.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        layout.setAlignment(LayoutAlignment.CENTER);
        layout.addComponent(view);
        setContentCustomComponent(layout);
        setTransparent(true);
    }

    public Component createCompnent(/*@NonNull*/ LayoutScatter scatter, ComponentContainer container,
                                                 PacMap savedInstanceState) {
        int viewRes =  ResourceTable.Layout_mdtp_easy_time_picker_dialog_v2;
        Component view = scatter.parse(viewRes, container,false);
        KeyboardListener keyboardListener = new KeyboardListener();
        view.findComponentById(ResourceTable.Id_mdtp_easy_time_picker_dialog).setKeyEventListener(keyboardListener);

        if (mThemeDark) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromRgbaInt(ResourceTable.Color_mdtp_date_picker_text_normal));
            view.setBackground(shapeElement);
            view.findComponentById(ResourceTable.Id_custom_dialog).setBackground(shapeElement);

            Text set_time_title = (Text) view.findComponentById(ResourceTable.Id_set_time_title);
            Text ed_hours = (Text) view.findComponentById(ResourceTable.Id_ed_hours);
            Text colon = (Text) view.findComponentById(ResourceTable.Id_colon);
            Text ed_minute = (Text) view.findComponentById(ResourceTable.Id_ed_minute);
            Text radio_button_1 = (Text) view.findComponentById(ResourceTable.Id_radio_button_1);
            Text radio_button_2 = (Text) view.findComponentById(ResourceTable.Id_radio_button_2);
            Text hours = (Text) view.findComponentById(ResourceTable.Id_hours);
            Text minutes = (Text) view.findComponentById(ResourceTable.Id_minutes);
            set_time_title.setTextColor(Color.WHITE);  ed_hours.setTextColor(Color.WHITE);
            colon.setTextColor(Color.WHITE);  ed_minute.setTextColor(Color.WHITE);
            radio_button_1.setTextColor(Color.WHITE);  radio_button_2.setTextColor(Color.WHITE);
            hours.setTextColor(Color.WHITE);  minutes.setTextColor(Color.WHITE);
        }
        // If an accent color has not been set manually, get it from the context
        if (mAccentColor == null) {
            mAccentColor = 0xffff4081;
        }

        // if theme mode has not been set by java code, check if it is specified in Style.xml
        if (!mThemeDarkChanged) {
            mThemeDark = Utils.isDarkTheme(context, mThemeDark);
        }

        Text ed_hours  = (Text) view.findComponentById(ResourceTable.Id_ed_hours);
        Text ed_minute = (Text) view.findComponentById(ResourceTable.Id_ed_minute);

        ed_hours.setText(String.valueOf(hour));
        ed_minute.setText(String.valueOf(minutes));


        Image change = (Image) view.findComponentById(ResourceTable.Id_key_borad);
        change.setVisibility(Component.VISIBLE);
        change.setPixelMap(ResourceTable.Media_time);
        change.setClickedListener(v -> {
            tryVibrate();
            cancel();
            mCallback.onTimeShow(this,showTime);
        });

        RadioContainer radioContainer = (RadioContainer) view.findComponentById(ResourceTable.Id_radio_container);
        radioContainer.mark(calendar.get(Calendar.AM_PM) == 0 ? 0 : 1);
        radioContainer.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int index) {
                am_pm = index == 0 ? "上午" : "下午";
            }
        });

        mHapticFeedbackController = new HapticFeedbackController(context);
        mOkButton = (Button) view.findComponentById(ResourceTable.Id_mdtp_ok);
        mOkButton.setClickedListener(v -> {
            ResourceUtils.printChilds(getContentCustomComponent(), "");
            notifyOnDateListener();
            dismiss();
        });

        mOkButton.setKeyEventListener(keyboardListener);
        if (mOkString != null) {
            mOkButton.setText(mOkString);
        } else {
            mOkButton.setText(mOkResid);
        }

        mCancelButton = (Button) view.findComponentById(ResourceTable.Id_mdtp_cancel);
        mCancelButton.setClickedListener(v -> {
            tryVibrate();
            cancel();
        });
        if (mCancelString != null) mCancelButton.setText(mCancelString);
        else mCancelButton.setText(mCancelResid);
        mCancelButton.setVisibility(isCancelable() ? Component.VISIBLE : Component.HIDE);
        if (mThemeDark) {
            ResourceUtils.setBackgroundColor(view.findComponentById(ResourceTable.Id_mdtp_time_display_background), Color.getIntColor("#484848"));
        }
        ResourceUtils.setBackgroundColor(view.findComponentById(ResourceTable.Id_mdtp_time_display), mAccentColor);

        // Button text can have a different color
        if (mOkColor == null) mOkColor = mAccentColor;
        mOkButton.setTextColor(new Color(mOkColor));
        if (mCancelColor == null) mCancelColor = mAccentColor;
        mCancelButton.setTextColor(new Color(mCancelColor));
        int backgroundColor = ResourceUtils.getColorValue(context, ResourceTable.Color_mdtp_background_color);
        int darkBackgroundColor = ResourceUtils.getColorValue(context, ResourceTable.Color_mdtp_light_gray);
        ResourceUtils.setBackgroundColor(view.findComponentById(ResourceTable.Id_mdtp_easy_time_picker_dialog), mThemeDark ? darkBackgroundColor : backgroundColor);
        return view;
    }

    @Override
    protected void onWindowConfigUpdated(WindowManager.LayoutConfig configParam) {
        super.onWindowConfigUpdated(configParam);
    }

    @Override
    public void onShow() {
        super.onShow();
        mHapticFeedbackController.start();
    }

    @Override
    public void onHide() {
        super.onHide();
        mHapticFeedbackController.stop();
        if (mDismissOnPause) dismiss();
    }

    @Override
    public void tryVibrate() {
        if (mVibrate) mHapticFeedbackController.tryVibrate();
    }

    /**
     * Called by the picker for updating the header display.
     */
    @Override
    public void onValueSelected(Timepoint newValue) {
    }

    @Override
    public void advancePicker(int index) {

    }

    @Override
    public void enablePicker() {
    }

    public boolean isOutOfRange(Timepoint current) {
        return isOutOfRange(current, SECOND_INDEX);
    }

    @Override
    public boolean isOutOfRange(Timepoint current, int index) {
        return false;
    }

    @Override
    public boolean isAmDisabled() {
        return false;
    }

    @Override
    public boolean isPmDisabled() {
        return false;
    }

    /**
     * Round a given Timepoint to the nearest valid Timepoint
     * @param time Timepoint - The timepoint to round
     * @return Timepoint - The nearest valid Timepoint
     */
    private Timepoint roundToNearest(/*@NonNull*/ Timepoint time) {
        return roundToNearest(time, null);
    }

    @Override
    public Timepoint roundToNearest(/*@NonNull*/ Timepoint time, /*@Nullable*/ Timepoint.TYPE type) {
        return null;
    }

    /**
     * Get the configured resolution of the current picker in terms of Timepoint components
     * @return Timepoint.TYPE (hour, minute or second)
     */
    /*@NonNull*/ Timepoint.TYPE getPickerResolution() {
        if (mEnableSeconds) return Timepoint.TYPE.SECOND;
        if (mEnableMinutes) return Timepoint.TYPE.MINUTE;
        return Timepoint.TYPE.HOUR;
    }

    private class KeyboardListener implements Component.KeyEventListener {
        @Override
        public boolean onKeyEvent(Component component, KeyEvent event) {
            if (!event.isKeyDown()) {
//                    return processKeyUp(event.getKeyCode());
            }
            return false;
        }
    }

    public void notifyOnDateListener() {
        if (mCallback != null) {
            mCallback.onEasyTimeSet(this, year,month,day,hour, minutes,am_pm);
        }
    }

    @Override
    public void onBackground(DialogInterface dialogInterface) {
        if (mDismissOnPause) {
            dismiss();
        }
    }
}
