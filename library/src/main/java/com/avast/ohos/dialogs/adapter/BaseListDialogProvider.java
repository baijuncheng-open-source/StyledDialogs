/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.ohos.dialogs.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.app.Context;

public abstract class BaseListDialogProvider extends RecycleItemProvider {

    public Context mContext = null;
    public LayoutScatter mLayoutScatter = null;
    public int mItemLayoutId;
    public CharSequence[] mCharSequences = null;

    public BaseListDialogProvider(Context context, int itemLayoutId, CharSequence[] charSequences) {
        mContext = context;
        mLayoutScatter = LayoutScatter.getInstance(context);
        mItemLayoutId = itemLayoutId;
        mCharSequences = charSequences;
    }

    @Override
    public int getCount() {
        return mCharSequences.length;
    }

    @Override
    public Object getItem(int i) {
        return mCharSequences[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        return initView(i, component, componentContainer);
    }

    protected abstract Component initView(int i, Component component, ComponentContainer componentContainer);
}
