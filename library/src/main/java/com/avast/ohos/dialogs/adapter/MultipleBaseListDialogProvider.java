/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.avast.ohos.dialogs.adapter;

import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.app.Context;
import com.avast.ohos.dialogs.ResourceTable;

import java.util.ArrayList;
import java.util.Arrays;

public class MultipleBaseListDialogProvider extends BaseListDialogProvider {
    private ArrayList<Integer> mCheckedItems = new ArrayList<>();
    private boolean isDarkTheme;
    public MultipleBaseListDialogProvider(Context context, int itemLayoutId, CharSequence[] charSequences, int[] checkedItems,boolean is) {
        super(context, itemLayoutId, charSequences);
        isDarkTheme = is;
        for (int i = 0; i < checkedItems.length; i++) {
            mCheckedItems.add(checkedItems[i]);
        }
    }

    @Override
    protected Component initView(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        if (component == null) {
            viewHolder = new ViewHolder();
            component = mLayoutScatter.parse(mItemLayoutId, null, false);
            viewHolder.checkbox = (Checkbox) component.findComponentById(ResourceTable.Id_sdl_text);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }
        viewHolder.checkbox.setText((String) getItem(i));
        if (isDarkTheme) {
            viewHolder.checkbox.setTextColor(Color.WHITE);
        }
        if (mCheckedItems.contains(i)) {
            viewHolder.checkbox.setChecked(true);
        } else {
            viewHolder.checkbox.setChecked(false);
        }
        return component;
    }

    private static class ViewHolder {
        private Checkbox checkbox;
    }

    public void setCheckedItem(int position) {
        if (mCheckedItems.contains(position)) {
            for (int i = 0; i < mCheckedItems.size(); i++) {
                if (mCheckedItems.get(i) == position) {
                    mCheckedItems.remove(i);
                }
            }
        } else {
            mCheckedItems.add(position);
        }
        notifyDataChanged();
    }

    public int[] getCheckedItem() {
        int[] array = new int[mCheckedItems.size()];
        for (int i = 0; i < mCheckedItems.size(); i++) {
            array[i] = mCheckedItems.get(i);
        }
        Arrays.sort(array);
        return array;
    }
}
