/*
 * Copyright 2013 Inmite s.r.o. (www.inmite.eu).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.ohos.dialogs.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.avast.ohos.dialogs.fragment.SimpleDialogFragment;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.utils.PacMap;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.ScrollView;
import ohos.agp.components.StackLayout;
import com.avast.ohos.dialogs.ResourceTable;
import com.avast.ohos.dialogs.iface.ISimpleDialogCancelListener;
import com.avast.ohos.dialogs.util.TypefaceHelper;

/**
 * Base dialog fragment for all your dialogs, styleable and same design on ohos.
 *
 * @author David Vávra (david@inmite.eu)
 */
public abstract class BaseDialogFragment extends CommonDialog {

    protected int mRequestCode;
    protected Context mContext;
    protected boolean mCancelableOnTouchOutside;
    protected PacMap mPacMap;
    protected boolean isDarkTheme = false;

    public BaseDialogFragment(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setDarkTheme(boolean setDarkTheme) {
        isDarkTheme = setDarkTheme;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        setTransparent(true);
        PacMap args = getArguments();
        if (args != null) {
            mCancelableOnTouchOutside = args.getBooleanValue("cancelable_oto");
            isDarkTheme = args.getBooleanValue("usedarktheme");
            mRequestCode = args.getIntValue("request_code", 0);
        }
        Builder builder = new Builder(mContext,isDarkTheme);
        setContentCustomComponent(build(builder).create());
        setDialogListener(new DialogListener() {
            @Override
            public boolean isTouchOutside() {
                if (mCancelableOnTouchOutside) {
                    for (ISimpleDialogCancelListener listener : getCancelListeners()) {
                        listener.onCancelled(mRequestCode);
                    }
                    remove();
                }
                return true;
            }
        });

    }

    /**
     * Key method for using {@link BaseDialogFragment}.
     * Customized dialogs need to be set up via provided builder.
     *
     * @param initialBuilder Provided builder for setting up customized dialog
     * @return Updated builder
     */
    protected abstract Builder build(Builder initialBuilder);

    @Override
    public void onShow() {
        super.onShow();
        if (getContentCustomComponent() != null) {
            ScrollView vMessageScrollView = (ScrollView) getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_message_scrollview);
            StackLayout vCustomViewNoScrollView = (StackLayout) getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_custom);
            ListContainer vListView = (ListContainer) getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_list);

            boolean messageScrollable = isScrollable(vMessageScrollView);
            boolean customViewNoScrollViewScrollable = false;
            if (vCustomViewNoScrollView.getChildCount() > 0) {
                Component firstChild = vCustomViewNoScrollView.getComponentAt(0);
                if (firstChild instanceof ComponentContainer) {
                    customViewNoScrollViewScrollable = isScrollable((ComponentContainer) firstChild);
                }
            }
            boolean listViewScrollable = isScrollable(vListView);
            boolean scrollable = messageScrollable || customViewNoScrollViewScrollable || listViewScrollable;
            modifyButtonsBasedOnScrollableContent(scrollable);
        }
    }

    protected void setArguments(PacMap pacMap) {
        mPacMap = pacMap;
    }

    protected PacMap getArguments() {
        return mPacMap;
    }
    /**
     * Get dialog cancel listeners.
     * There might be more than one cancel listener.
     *
     * @return Dialog cancel listeners
     * @since 2.1.0
     */
    protected List<ISimpleDialogCancelListener> getCancelListeners() {
        return getDialogListeners(ISimpleDialogCancelListener.class);
    }

    /**
     * Utility method for acquiring all listeners of some type for current instance of DialogFragment
     *
     * @param listenerInterface Interface of the desired listeners
     * @return Unmodifiable list of listeners
     * @since 2.1.0
     */
    @SuppressWarnings("unchecked")
    protected <T> List<T> getDialogListeners(Class<T> listenerInterface) {
        List<T> listeners = new ArrayList<T>(1);
        if (mContext != null && listenerInterface.isAssignableFrom(((T) mContext).getClass())) {
            listeners.add((T) mContext);
        }
        return Collections.unmodifiableList(listeners);
    }

    /**
     * Button divider should be shown only if the content is scrollable.
     */
    private void modifyButtonsBasedOnScrollableContent(boolean scrollable) {
        if (getContentCustomComponent() == null) {
            return;
        }
        Component vButtonDivider = getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_button_divider);
        Component vButtonsBottomSpace = getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_buttons_bottom_space);
        Component vDefaultButtons = getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_buttons_default);
        Component vStackedButtons = getContentCustomComponent().findComponentById(ResourceTable.Id_sdl_buttons_stacked);
        if (vDefaultButtons.getVisibility() == Component.HIDE && vStackedButtons.getVisibility() == Component.HIDE) {
            // no buttons
            vButtonDivider.setVisibility(Component.HIDE);
            vButtonsBottomSpace.setVisibility(Component.HIDE);
        } else if (scrollable) {
            vButtonDivider.setVisibility(Component.VISIBLE);
            vButtonsBottomSpace.setVisibility(Component.HIDE);
        } else {
            vButtonDivider.setVisibility(Component.HIDE);
            vButtonsBottomSpace.setVisibility(Component.VISIBLE);
        }
    }

    private boolean isScrollable(ComponentContainer listView) {
        int totalHeight = 0;
        for (int i = 0; i < listView.getChildCount(); i++) {
            Component component = listView.getComponentAt(i);
            if (null != component) {
                totalHeight += component.getHeight();
            }
        }
        return listView.getHeight() < totalHeight;
    }

    /**
     * Custom dialog builder
     */
    protected static class Builder {

        private final Context mContext;

        private final LayoutScatter mInflater;

        private String mTitle;

        private String mPositiveButtonText;

        private Component.ClickedListener mPositiveButtonListener;

        private String mNegativeButtonText;

        private Component.ClickedListener mNegativeButtonListener;

        private String mNeutralButtonText;

        private Component.ClickedListener mNeutralButtonListener;

        private String mMessage;

        private Component mCustomView;

        private RecycleItemProvider mListAdapter;

        private ListContainer.ItemClickedListener mOnItemClickListener;

        private boolean isDarkTheme = false;

        private boolean misJayne = false;

        public Builder(Context context) {
            this.mContext = context;
            this.mInflater = LayoutScatter.getInstance(mContext);
        }
        public Builder(Context context,boolean isDarkTheme) {
            this.mContext = context;
            this.mInflater = LayoutScatter.getInstance(mContext);
            this.isDarkTheme = isDarkTheme;
        }

        public LayoutScatter getLayoutInflater() {
            return mInflater;
        }

        public Builder setTitle(int titleId) {
            try {
                mTitle = mContext.getResourceManager().getElement(titleId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mTitle = "";
            }
            return this;
        }

        public Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public Builder setPositiveButton(int textId, final Component.ClickedListener listener) {
            try {
                mPositiveButtonText = mContext.getResourceManager().getElement(textId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mPositiveButtonText = "";
            }
            mPositiveButtonListener = listener;
            return this;
        }

        public Builder setPositiveButton(String text, final Component.ClickedListener listener) {
            mPositiveButtonText = text;
            mPositiveButtonListener = listener;
            return this;
        }

        public Builder setNegativeButton(int textId, final Component.ClickedListener listener) {
            try {
                mNegativeButtonText = mContext.getResourceManager().getElement(textId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mNegativeButtonText = "";
            }
            mNegativeButtonListener = listener;
            return this;
        }

        public Builder setNegativeButton(String text, final Component.ClickedListener listener) {
            mNegativeButtonText = text;
            mNegativeButtonListener = listener;
            return this;
        }

        public Builder setNeutralButton(int textId, final Component.ClickedListener listener) {
            try {
                mNeutralButtonText = mContext.getResourceManager().getElement(textId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mNeutralButtonText = "";
            }
            mNegativeButtonListener = listener;
            return this;
        }

        public Builder setNeutralButton(String text, final Component.ClickedListener listener) {
            mNeutralButtonText = text;
            mNeutralButtonListener = listener;
            return this;
        }

        public Builder setMessage(int messageId) {
            try {
                mMessage = mContext.getResourceManager().getElement(messageId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mMessage = "";
            }
            return this;
        }

        public Builder setMessage(String message) {
            mMessage = message;
            return this;
        }

        public Builder setItems(RecycleItemProvider listAdapter, final ListContainer.ItemClickedListener listener) {
            mListAdapter = listAdapter;
            mOnItemClickListener = listener;
            return this;
        }

        public Builder setView(Component view,boolean isJayne) {
            mCustomView = view;
            misJayne = isJayne;
            return this;
        }

        public Component create() {
            DirectionalLayout content = (DirectionalLayout) mInflater.parse(ResourceTable.Layout_sdl_dialog, null, false);
            Text vTitle = (Text) content.findComponentById(ResourceTable.Id_sdl_title);
            Text vMessage = (Text) content.findComponentById(ResourceTable.Id_sdl_message);
            if (isDarkTheme) {
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromRgbaInt(ResourceTable.Color_mdtp_light_gray));
                content.setBackground(shapeElement);
                vMessage.setTextColor(Color.WHITE);
            }

            StackLayout vCustomView = (StackLayout) content.findComponentById(ResourceTable.Id_sdl_custom);
            Button vPositiveButton = (Button) content.findComponentById(ResourceTable.Id_sdl_button_positive);
            Button vNegativeButton = (Button) content.findComponentById(ResourceTable.Id_sdl_button_negative);
            Button vNeutralButton = (Button) content.findComponentById(ResourceTable.Id_sdl_button_neutral);
            Button vPositiveButtonStacked = (Button) content.findComponentById(ResourceTable.Id_sdl_button_positive_stacked);
            Button vNegativeButtonStacked = (Button) content.findComponentById(ResourceTable.Id_sdl_button_negative_stacked);
            Button vNeutralButtonStacked = (Button) content.findComponentById(ResourceTable.Id_sdl_button_neutral_stacked);
            Component vButtonsDefault = content.findComponentById(ResourceTable.Id_sdl_buttons_default);
            Component vButtonsStacked = content.findComponentById(ResourceTable.Id_sdl_buttons_stacked);
            ListContainer vList = (ListContainer) content.findComponentById(ResourceTable.Id_sdl_list);

            Font regularFont = TypefaceHelper.get(mContext, "Roboto-Regular");
            Font mediumFont = TypefaceHelper.get(mContext, "Roboto-Medium");

            set(vTitle, mTitle, mediumFont);
            set(vMessage, mMessage, regularFont);
            setPaddingOfTitleAndMessage(vTitle, vMessage);

            if (mCustomView != null) {
                if (misJayne) {
                    mCustomView.setWidth(600);
                    mCustomView.setHeight(600);
                    mCustomView.setMarginLeft(200);
                    mCustomView.setMarginRight(200);
                }
                vCustomView.addComponent(mCustomView);
            }
            if (mListAdapter != null) {
                vList.setItemProvider(mListAdapter);
                vList.setItemClickedListener(mOnItemClickListener);
            }

            if (shouldStackButtons()) {
                set(vPositiveButtonStacked, mPositiveButtonText, mediumFont, mPositiveButtonListener);
                set(vNegativeButtonStacked, mNegativeButtonText, mediumFont, mNegativeButtonListener);
                set(vNeutralButtonStacked, mNeutralButtonText, mediumFont, mNeutralButtonListener);
                vButtonsDefault.setVisibility(Component.HIDE);
                vButtonsStacked.setVisibility(Component.VISIBLE);
            } else {
                set(vPositiveButton, mPositiveButtonText, mediumFont, mPositiveButtonListener);
                set(vNegativeButton, mNegativeButtonText, mediumFont, mNegativeButtonListener);
                set(vNeutralButton, mNeutralButtonText, mediumFont, mNeutralButtonListener);
                vButtonsDefault.setVisibility(Component.VISIBLE);
                vButtonsStacked.setVisibility(Component.HIDE);
            }
            if (SimpleDialogFragment.isTextEmpty(mPositiveButtonText) && SimpleDialogFragment.isTextEmpty(mNegativeButtonText) && SimpleDialogFragment.isTextEmpty
                    (mNeutralButtonText)) {
                vButtonsDefault.setVisibility(Component.HIDE);
            }

            return content;
        }

        /**
         * Padding is different if there is only title, only message or both.
         */
        private void setPaddingOfTitleAndMessage(Text vTitle, Text vMessage) {
            int grid6 = 0;
            int grid4 = 0;
            try {
                grid6 = (int) mContext.getResourceManager().getElement(ResourceTable.Float_grid_6).getFloat();
                grid4 = (int) mContext.getResourceManager().getElement(ResourceTable.Float_grid_4).getFloat();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!SimpleDialogFragment.isTextEmpty(mTitle) && !SimpleDialogFragment.isTextEmpty(mMessage)) {
                vTitle.setPadding(grid6, grid6, grid6, grid4);
                vMessage.setPadding(grid6, 0, grid6, grid4);
            } else if (SimpleDialogFragment.isTextEmpty(mTitle)) {
                vMessage.setPadding(grid6, grid4, grid6, grid4);
            } else if (SimpleDialogFragment.isTextEmpty(mMessage)) {
                vTitle.setPadding(grid6, grid6, grid6, grid4);
            }
        }

        private boolean shouldStackButtons() {
            return shouldStackButton(mPositiveButtonText) || shouldStackButton(mNegativeButtonText)
                    || shouldStackButton(mNeutralButtonText);
        }

        private boolean shouldStackButton(String text) {
            final int MAX_BUTTON_CHARS = 12; // based on observation, could be done better with measuring widths
            return text != null && text.length() > MAX_BUTTON_CHARS;
        }

        private void set(Button button, String text, Font font, Component.ClickedListener listener) {
            set(button, text, font);
            if (listener != null) {
                button.setClickedListener(listener);
            }
        }

        private void set(Text textView, String text, Font font) {
            if (text != null) {
                textView.setText(text);
                textView.setFont(font);
            } else {
                textView.setVisibility(Component.HIDE);
            }
        }
    }
}