/*
 * Copyright 2013 Inmite s.r.o. (www.inmite.eu).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.ohos.dialogs.fragment;

import java.util.List;

import com.avast.ohos.dialogs.core.BaseDialogBuilder;
import com.avast.ohos.dialogs.core.BaseDialogFragment;
import com.avast.ohos.dialogs.iface.INegativeButtonDialogListener;
import com.avast.ohos.dialogs.iface.INeutralButtonDialogListener;
import com.avast.ohos.dialogs.iface.IPositiveButtonDialogListener;
import ohos.app.Context;
import ohos.agp.components.Component;
import ohos.utils.PacMap;

/**
 * Dialog for displaying simple message, message with title or message with title and two buttons. Implement {@link
 * com.avast.ohos.dialogs.iface.ISimpleDialogListener} in your Fragment or Activity to rect on positive and negative button clicks. This class can
 * be extended and more parameters can be added in overridden build() method.
 *
 * @author David Vávra (david@inmite.eu)
 */
public class SimpleDialogFragment extends BaseDialogFragment {

    public SimpleDialogFragment(Context context) {
        super(context);
    }

    public static SimpleDialogBuilder createBuilder(Context context) {
        return new SimpleDialogBuilder(context,SimpleDialogFragment.class);
    }

    public static boolean isTextEmpty(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }
    /**
     * Key method for extending {@link SimpleDialogFragment}.
     * Children can extend this to add more things to base builder.
     */
    @Override
    protected Builder build(Builder builder) {
        final String title = (String) getTitle();
        if (!isTextEmpty(title)) {
            builder.setTitle(title);
        }

        final String message = (String) getMessage();
        if (!isTextEmpty(message)) {
            builder.setMessage(message);
        }

        final String positiveButtonText = (String) getPositiveButtonText();
        if (!isTextEmpty(positiveButtonText)) {
            builder.setPositiveButton(positiveButtonText, new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    for (IPositiveButtonDialogListener listener : getPositiveButtonDialogListeners()) {
                        listener.onPositiveButtonClicked(mRequestCode);
                    }
                    remove();
                }
            });
        }

        final String negativeButtonText = (String) getNegativeButtonText();
        if (!isTextEmpty(negativeButtonText)) {
            builder.setNegativeButton(negativeButtonText, new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    for (INegativeButtonDialogListener listener : getNegativeButtonDialogListeners()) {
                        listener.onNegativeButtonClicked(mRequestCode);
                    }
                    remove();
                }
            });
        }

        final String neutralButtonText = (String) getNeutralButtonText();
        if (!isTextEmpty(neutralButtonText)) {
            builder.setNeutralButton(neutralButtonText, new Component.ClickedListener() {
                @Override
                public void onClick(Component view) {
                    for (INeutralButtonDialogListener listener : getNeutralButtonDialogListeners()) {
                        listener.onNeutralButtonClicked(mRequestCode);
                    }
                    remove();
                }
            });
        }

        return builder;
    }

    protected CharSequence getMessage() {
        return getArguments().getString("message");
    }

    protected CharSequence getTitle() {
        return getArguments().getString("title");
    }

    protected CharSequence getPositiveButtonText() {
        return getArguments().getString("positive_button");
    }

    protected CharSequence getNegativeButtonText() {
        return getArguments().getString("negative_button");
    }

    protected CharSequence getNeutralButtonText() {
        return getArguments().getString("neutral_button");
    }

    /**
     * Get positive button dialog listeners.
     * There might be more than one listener.
     *
     * @return Dialog listeners
     * @since 2.1.0
     */
    protected List<IPositiveButtonDialogListener> getPositiveButtonDialogListeners() {
        return getDialogListeners(IPositiveButtonDialogListener.class);
    }

    /**
     * Get negative button dialog listeners.
     * There might be more than one listener.
     *
     * @return Dialog listeners
     * @since 2.1.0
     */
    protected List<INegativeButtonDialogListener> getNegativeButtonDialogListeners() {
        return getDialogListeners(INegativeButtonDialogListener.class);
    }

    /**
     * Get neutral button dialog listeners.
     * There might be more than one listener.
     *
     * @return Dialog listeners
     * @since 2.1.0
     */
    protected List<INeutralButtonDialogListener> getNeutralButtonDialogListeners() {
        return getDialogListeners(INeutralButtonDialogListener.class);
    }


    public static class SimpleDialogBuilder extends BaseDialogBuilder<SimpleDialogBuilder> {

        private String mTitle;
        private String mMessage;
        private String mPositiveButtonText;
        private String mNegativeButtonText;
        private String mNeutralButtonText;

        protected SimpleDialogBuilder(Context context, Class<? extends SimpleDialogFragment> clazz) {
            super(context, clazz);
        }

        @Override
        protected SimpleDialogBuilder self() {
            return this;
        }

        public SimpleDialogBuilder setTitle(int titleResourceId) {
            try {
                mTitle = mContext.getResourceManager().getElement(titleResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mTitle = "";
            }
            return this;
        }

        public SimpleDialogBuilder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public SimpleDialogBuilder setMessage(int messageResourceId) {
            try {
                mMessage = mContext.getResourceManager().getElement(messageResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mMessage = "";
            }
            return this;
        }

        /**
         * Allow to set resource string with HTML formatting and bind %s,%i.
         * This is workaround for https://code.google.com/p/android/issues/detail?id=2923
         */
        public SimpleDialogBuilder setMessage(String message) {
            mMessage = message;
            return this;
        }

//        public SimpleDialogBuilder setMessage(CharSequence message) {
//            mMessage = message;
//            return this;
//        }

        public SimpleDialogBuilder setPositiveButtonText(int textResourceId) {
            try {
                mPositiveButtonText = mContext.getResourceManager().getElement(textResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mPositiveButtonText = "";
            }
            return this;
        }

        public SimpleDialogBuilder setPositiveButtonText(String text) {
            mPositiveButtonText = text;
            return this;
        }

        public SimpleDialogBuilder setNegativeButtonText(int textResourceId) {
            try {
                mNegativeButtonText = mContext.getResourceManager().getElement(textResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mNegativeButtonText = "";
            }
            return this;
        }

        public SimpleDialogBuilder setNegativeButtonText(String text) {
            mNegativeButtonText = text;
            return this;
        }

        public SimpleDialogBuilder setNeutralButtonText(int textResourceId) {
            try {
                mNeutralButtonText = mContext.getResourceManager().getElement(textResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mNeutralButtonText = "";
            }
            return this;
        }

        public SimpleDialogBuilder setNeutralButtonText(String text) {
            mNeutralButtonText = text;
            return this;
        }
        public SimpleDialogBuilder setDarkTheme(boolean setDarkTheme) {

            return this;
        }

        @Override
        protected PacMap prepareArguments() {
            PacMap args = new PacMap();
            args.putString("message", mMessage);
            args.putString("title", mTitle);
            args.putString("positive_button", mPositiveButtonText);
            args.putString("negative_button", mNegativeButtonText);
            args.putString("neutral_button", mNeutralButtonText);
            return args;
        }
    }
}
