package com.avast.ohos.dialogs.fragment;


import com.avast.ohos.dialogs.core.BaseDialogBuilder;
import com.avast.ohos.dialogs.core.BaseDialogFragment;
import ohos.agp.utils.Color;
import ohos.app.Context;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

import com.avast.ohos.dialogs.ResourceTable;

/**
 * Simple progress dialog that shows indeterminate progress bar together with message and dialog title (optional).<br/>
 * <p>
 * To show the dialog, start with {@link #createBuilder(ohos.app.Context)}.
 * </p>
 * <p>
 * Dialog can be cancelable - to listen to cancellation, activity or target fragment must implement {@link com.avast.ohos.dialogs.iface.ISimpleDialogCancelListener}
 * </p>
 *
 * @author Tomas Vondracek
 */
public class ProgressDialogFragment extends BaseDialogFragment {

    protected final  String ARG_MESSAGE = "message";
    protected final  String ARG_TITLE = "title";
    private AnimatorValue mAnimator;

    public ProgressDialogFragment(Context context) {
        super(context);
    }


    public static ProgressDialogBuilder createBuilder(Context context) {
        return new ProgressDialogBuilder(context);
    }

    @Override
    protected Builder build(Builder builder) {
        final LayoutScatter inflater = builder.getLayoutInflater();
        final Component view = inflater.parse(ResourceTable.Layout_sdl_progress, null, false);
        final Text tvMessage = (Text) view.findComponentById(ResourceTable.Id_sdl_message);
        final Image imgProgress = (Image) view.findComponentById(ResourceTable.Id_sdl_progress);

        int setPixelMap = getArguments().getIntValue("image");
        if (setPixelMap != -1) {
            imgProgress.setPixelMap(setPixelMap);
        }

        mAnimator = new AnimatorValue();
        mAnimator.setDuration(2000);
        mAnimator.setDelay(1000);
        mAnimator.setLoopedCount(Animator.INFINITE);
        mAnimator.setCurveType(Animator.CurveType.LINEAR);
        mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                imgProgress.setRotation(value * 360);
            }
        });
        mAnimator.start();
        tvMessage.setText(getArguments().getString("message"));
        builder.setView(view,false);
        builder.setTitle(getArguments().getString("title"));
        if (isDarkTheme) {
            tvMessage.setTextColor(Color.WHITE);
        }
        return builder;
    }

    @Override
    protected void onHide() {
        super.onHide();
        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.stop();
        }
    }

    public static class ProgressDialogBuilder extends BaseDialogBuilder<ProgressDialogBuilder> {

        private String mTitle;
        private String mMessage;
        private int mProgressImage = -1;
        protected ProgressDialogBuilder(Context context) {
            super(context, ProgressDialogFragment.class);
        }

        @Override
        protected ProgressDialogBuilder self() {
            return this;
        }

        public ProgressDialogBuilder setTitle(int titleResourceId) {
            try {
                mTitle = mContext.getResourceManager().getElement(titleResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mTitle = "";
            }
            return this;
        }

        public ProgressDialogBuilder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public ProgressDialogBuilder setMessage(int messageResourceId) {
            try {
                mMessage = mContext.getResourceManager().getElement(messageResourceId).getString();
            } catch (Exception e) {
                e.printStackTrace();
                mMessage = "";
            }
            return this;
        }

        public ProgressDialogBuilder setMessage(String message) {
            mMessage = message;
            return this;
        }


        public ProgressDialogBuilder setProgressImage(int progressImage) {
            mProgressImage = progressImage;
            return this;
        }

        @Override
        protected PacMap prepareArguments() {
            PacMap args = new PacMap();
            args.putString("message", mMessage);
            args.putString("title", mTitle);
            args.putIntValue("image", mProgressImage);
            return args;
        }
    }
}
