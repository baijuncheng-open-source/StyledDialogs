/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.ohos.dialogs.date;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * A DayPickerView customized for {@link com.avast.ohos.dialogs.date.SimpleMonthAdapter}
 */
public class SimpleDayPickerView extends com.avast.ohos.dialogs.date.DayPickerView {

    public SimpleDayPickerView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public SimpleDayPickerView(Context context, com.avast.ohos.dialogs.date.DatePickerController controller) {
        super(context, controller);
    }

    @Override
    public com.avast.ohos.dialogs.date.MonthAdapter createMonthAdapter(com.avast.ohos.dialogs.date.DatePickerController controller) {
        return new com.avast.ohos.dialogs.date.SimpleMonthAdapter(controller);
    }

}
