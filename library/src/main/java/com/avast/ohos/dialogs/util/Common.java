package com.avast.ohos.dialogs.util;

import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class Common {
    public static void  toastDialog (String s, Context context) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setDuration(200);
        toastDialog.setText(s);
        toastDialog.show();
    }
}
