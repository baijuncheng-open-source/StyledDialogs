package com.avast.ohos.dialogs.util;

import ohos.app.Context;
import java.util.HashMap;
import ohos.agp.text.Font;

public class TypefaceHelper {

    private static final HashMap<String, Font> cache = new HashMap<>();

    public static Font get(Context c, String name) {
        synchronized (cache) {
            if (!cache.containsKey(name)) {
                Font t = new Font.Builder(String.format("resources/rawfile/fonts/%s.ttf", name)).build();
                return t;
            }
            return cache.get(name);
        }
    }
}