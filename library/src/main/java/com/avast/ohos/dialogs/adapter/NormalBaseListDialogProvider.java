/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avast.ohos.dialogs.adapter;

import com.avast.ohos.dialogs.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class NormalBaseListDialogProvider extends BaseListDialogProvider {

    private boolean isDarkTheme;
    public NormalBaseListDialogProvider(Context context, int itemLayoutId, CharSequence[] charSequences,boolean is) {
        super(context, itemLayoutId, charSequences);
        isDarkTheme = is;
    }

    @Override
    protected Component initView(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        if (component == null) {
            viewHolder = new ViewHolder();
            component = mLayoutScatter.parse(mItemLayoutId, componentContainer, false);
            viewHolder.text = (Text) component.findComponentById(ResourceTable.Id_sdl_text);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }
        viewHolder.text.setText((String) getItem(i));
        if (isDarkTheme) {
            viewHolder.text.setTextColor(Color.WHITE);
        }
        return component;
    }

    private static class ViewHolder {
        private Text text;
    }


}
