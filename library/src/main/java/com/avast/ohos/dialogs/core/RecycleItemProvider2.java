package com.avast.ohos.dialogs.core;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.RecycleItemProvider;

import java.util.HashMap;

/**
 * 一个可以根据position获取Component的RecycleItemProvider
 * 用来解决ListContainer.getComponentAt返回始终为null的问题
 */
public abstract class RecycleItemProvider2 extends RecycleItemProvider {

    HashMap<Integer, Component> map = new HashMap();

    /**
     * 实现ListContainer的getComponentAt功能
     * @param position
     * @return
     */
    public Component getComponentAt(int position) {
        return map.get(position);
    }

    public int getComponentPosition(Component component) {
        for (Integer key : map.keySet()) {
            if (component == map.get(key)) {
                return key;
            }
        }
        return -1;
    }

    public abstract Component getComponent2(int position, Component convertComponent, ComponentContainer componentContainer);

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component component = getComponent2(position, convertComponent, componentContainer);
        if (component == convertComponent) {
            int pos = getComponentPosition(convertComponent);
            if (pos >= 0) {
                map.remove(pos);
            }
        }
        map.put(position, component);
        return component;
    }
}