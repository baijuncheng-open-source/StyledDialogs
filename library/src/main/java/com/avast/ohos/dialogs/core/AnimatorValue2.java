package com.avast.ohos.dialogs.core;

import ohos.agp.animation.AnimatorValue;
import java.util.ArrayList;
import java.util.List;

public class AnimatorValue2 extends AnimatorValue {
    List<ValueUpdateListener> mUpdateListeners;

    private String name;

    public String getName() {
        return name;
    }

    public AnimatorValue2 setName(String name) {
        this.name = name;
        return this;
    }

    public AnimatorValue2() {
        super();
        setValueUpdateListener(new ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (mUpdateListeners == null) {
                    return;
                }
                for (int i = 0; i < mUpdateListeners.size(); i++) {
                    ValueUpdateListener listener = mUpdateListeners.get(i);
                    if (listener != null) {
                        listener.onUpdate(animatorValue, v);
                    }
                }
            }
        });
    }

    public void addUpdateListener(ValueUpdateListener listener) {
        if (mUpdateListeners == null) {
            mUpdateListeners = new ArrayList<ValueUpdateListener>();
        }
        mUpdateListeners.add(listener);
    }

    public void removeAllUpdateListeners() {
        if (mUpdateListeners == null) {
            return;
        }
        mUpdateListeners.clear();
        mUpdateListeners = null;
    }
}
