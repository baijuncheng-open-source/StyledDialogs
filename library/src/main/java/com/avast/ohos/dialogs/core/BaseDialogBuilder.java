package com.avast.ohos.dialogs.core;

import ohos.app.Context;
import ohos.utils.PacMap;

/**
 * Internal base builder that holds common values for all dialog fragment builders.
 *
 * @author Tomas Vondracek
 */
public abstract class BaseDialogBuilder<T extends BaseDialogBuilder<T>> {

    public final String DEFAULT_TAG = "simple_dialog";
    private String mTag = DEFAULT_TAG;
    public final int DEFAULT_REQUEST_CODE = -42;
    private int mRequestCode = DEFAULT_REQUEST_CODE;
    protected final Context mContext;
    protected final Class<? extends BaseDialogFragment> mClass;
    private boolean mCancelable = true;
    private boolean mCancelableOnTouchOutside = true;
    private boolean mUseDarkTheme = false;
    private boolean mUseLightTheme = false;

    public BaseDialogBuilder(Context context, Class<? extends BaseDialogFragment> clazz) {
        mContext = context;
        mClass = clazz;
    }

    protected abstract T self();

    protected abstract PacMap prepareArguments();

    public T setCancelable(boolean cancelable) {
        mCancelable = cancelable;
        return self();
    }

    public T setTargetFragment(int requestCode) {
        mRequestCode = requestCode;
        return self();
    }

    public T setRequestCode(int requestCode) {
        mRequestCode = requestCode;
        return self();
    }

    public T setTag(String tag) {
        mTag = tag;
        return self();
    }

    public T useDarkTheme(boolean isDarkTheme) {
        mUseDarkTheme = isDarkTheme;
        return self();
    }

    private BaseDialogFragment create() {
        PacMap args = prepareArguments();

        args.putBooleanValue("cancelable_oto", mCancelableOnTouchOutside);
        args.putBooleanValue("usedarktheme", mUseDarkTheme);
        args.putBooleanValue("uselighttheme", mUseLightTheme);
        args.putIntValue("request_code", mRequestCode);

        BaseDialogFragment fragment = null;
        try {
            fragment = mClass.getDeclaredConstructor(Context.class).newInstance(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment != null) {
            fragment.setArguments(args);
        }

        return fragment;
    }

    public void show() {
        BaseDialogFragment fragment = create();
        if (fragment != null) {
            fragment.show();
        }
    }

}
